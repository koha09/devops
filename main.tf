# main.tf

terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
  # This DID work on Windows 11 with WSL2!
  host    = "npipe:////./pipe/docker_engine"
}
resource "docker_container" "mysql" {
  name  = "mysql-container"
  image = "mysql:latest"

  ports {
    internal = 3306
    external = 3306
  }

  env = [
    "MYSQL_ROOT_PASSWORD=your_password",
    "MYSQL_DATABASE=wordpress",
  ]

  restart = "unless-stopped"
}

resource "docker_container" "wordpress" {
  name  = "wordpress-container"
  image = "wordpress:latest"

  ports {
    internal = 80
    external = 8080
  }

  env = [
    "WORDPRESS_DB_HOST=localhost",
    "WORDPRESS_DB_USER=root",
    "WORDPRESS_DB_PASSWORD=your_password",
    "WORDPRESS_DB_NAME=wordpress",
  ]

  depends_on = [docker_container.mysql]

  restart = "unless-stopped"
}
